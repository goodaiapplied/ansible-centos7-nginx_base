
Ansible role for a default nginx installation
============

## Summary

This Ansible role has the following features

 - Installs nginx from a nginx-based repo for Centos7.
 - Enables simple vhost management through `sites-available` and `sites-enabled` symlinking.
 

## Role Variables

### Mandatory variables

None.

### Optional variables
```yaml
root_dir: /var/www
data_dir: /files
sites_available: /etc/nginx/sites-available
sites_enabled: /etc/nginx/sites-enabled
```